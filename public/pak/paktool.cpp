
#include <intrin.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "paktool.h"

extern "C" {

//----------------------------------------------------------------
// Command arguments

static bool PAKtool_parseByte(const char* chars, unsigned char* out)
{
	unsigned char c1 = chars[0];
	if (c1 >= '0' && c1 <= '9') {
		c1 = c1 - '0';
	}
	else if (c1 >= 'A' && c1 <= 'F') {
		c1 = c1 - 'A' + 10;
	}
	else if (c1 >= 'a' && c1 <= 'f') {
		c1 = c1 - 'a' + 10;
	}
	else {
		return false;
	}

	char c2 = chars[1];
	if (c2 >= '0' && c2 <= '9') {
		c2 = c2 - '0';
	}
	else if (c2 >= 'A' && c2 <= 'F') {
		c2 = c2 - 'A' + 10;
	}
	else if (c2 >= 'a' && c2 <= 'f') {
		c2 = c2 - 'a' + 10;
	}
	else {
		return false;
	}

	*out = (c1 << 4) | c2;
	return true;
}

bool PAKtool_parseKey(const char* keystr, PAK_key* key)
{
	// Enforce key length, must be exactly correct
	size_t numc = strlen(keystr);
	if (numc == (sizeof(PAK_key) * 2)) {
		// Parse per byte (two hex digits)
		for (unsigned int i = 0; i < sizeof(PAK_key); ++i) {
			const char* s = &keystr[i * 2];
			if (!PAKtool_parseByte(s, &key->bytes[i])) {
				// Failure! Invalid characters!
				printf("PAKtool: Invalid key format, expected [0-9A-Fa-f] got %c%c!\n", s[0], s[1]);
				return false;
			}
		}
		// OK! key parsed.
		return true;
	}
	else if (!numc) {
		// OK! key omitted.
		memset(key, 0, sizeof(PAK_key));
		return true;
	}
	else {
		// Failure! Wrong number of chars.
		printf("PAKtool: Invalid key size, expected %d bits, got %d!\n", sizeof(PAK_key) * 8, numc * 4);
		return false;
	}
}
bool PAKtool_parseKeyArg(PAKtool_args* args)
{
	if (args->i >= args->c || args->v[args->i][0] != '-') {
		// No key provided, print a warning but allow
		printf("PAKtool: No key provided! Using null key.\n");
		memset(&args->key, 0, sizeof(PAK_key));
		return true;
	}

	char* ks = args->v[args->i++];
	if (ks[0] == '-' && ks[1] == 'k') {
		ks += 2;
	}
	else if (ks[0] == '-' && ks[1] == '-' && ks[2] == 'k' && ks[3] == 'e' && ks[4] == 'y' && ks[5] == '=') {
		ks += 6;
	}
	else {
		// Invalid argument, expecting -k or --key=
		printf("PAKtool: Invalid argument!\n");
		return false;
	}

	return PAKtool_parseKey(ks, &args->key);
}


const PAKtool_cmd* PAKtool_parseCmd(const char* cmd)
{
	static const PAKtool_cmd cmds[] = {
		{ "touch", nullptr },
		{ "fsck", &PAKtool_fsck },
		{ "ls", &PAKtool_ls },
		{ "cat", &PAKtool_cat },
		{ "add", &PAKtool_add },
		{ "rm", &PAKtool_rm },
		{ "mv", &PAKtool_mv },
		{ "unpack", &PAKtool_unpack },
		{ "gc", &PAKtool_gc }
	};
	for (auto it = cmds, end = it + sizeof(cmds) / sizeof(cmds[0]); it != end; ++it) {
		if (!strcmp(cmd, it->str)) {
			return it;
		}
	}
	return nullptr;
}

int PAKtool_parse(PAKtool_args* args)
{
	if (args->c < 2) {
		printf(
			"PAKtool by CursedGhost - %s\n"
			"paktool PAKFILE [KEY] CMD [ARGS...]\n"
			"View readme.md for further instructions.\n", __DATE__);
		return 0;
	}

	// OLD STYLE ARGUMENTS
	args->cmd = PAKtool_parseCmd(args->v[args->i]);
	if (args->cmd) {
		args->i++;

		printf("PAKtool: Legacy argument ordering, for compatibility only!\n");

		// Try reading the key argument
		if (!PAKtool_parseKeyArg(args)) {
			return 1;
		}

		// Get the pak filename
		if (args->i >= args->c) {
			printf("PAKtool: Missing PAK file argument!\n");
			return 1;
		}
		args->file = args->v[args->i++];
	}
	// NEW STYLE ARGUMENTS
	else {
		// Get the pak filename
		args->file = args->v[args->i++];

		// Try reading the key argument
		if (!PAKtool_parseKeyArg(args)) {
			return 1;
		}

		// Need one more required argument
		if (args->i >= args->c) {
			printf("PAKtool: Missing command argument!\n");
			return 1;
		}

		// Get the subcommand
		args->cmd = PAKtool_parseCmd(args->v[args->i++]);
		if (!args->cmd) {
			printf("PAKtool: Unknown command argument!\n");
			return 1;
		}
	}
	return 0;
}
int PAKtool_invoke(PAKtool_args* args)
{
	int ret;
	if (!args->cmd->pfn) {
		// Special case, create the PAK file
		ret = PAKtool_touch(args);
	}
	else {
		ret = 1;

		// Try to open the PAK file
		FILE* file = fopen(args->file, "r+b");
		if (!file) {
			printf("PAKtool: PAK file not found!\n");
		}
		else {
			// Try to initialize the PAK file
			struct PAK* pak = PAK_file(&args->key, file);
			if (!pak) {
				printf("PAKtool: Invalid PAK file!\n");
			}
			else {
				// Verify integrity so we don't accidentally corrupt it
				// FIXME! This kinda makes the fsck command redundant...
				if (PAK_fsck(pak, 0)) {
					// Execute the command
					ret = args->cmd->pfn(args, pak);
				}
				else {
					printf("PAKtool: Wrong key or corrupted PAK file!\n");
				}

				// Cleanup
				PAK_close(pak);
			}
			fclose(file);
		}
	}
	return ret;
}

//----------------------------------------------------------------
// Command handlers


// Callback for each argument without editing
static int PAKtool_each(PAKtool_args* args, PAK* pak, int(*pfn)(PAK* pak, char* filename))
{
	int err = 0;

	if (args->i == args->c) {
		// No arguments to process, this is most likely not intended
		printf("PAKtool: No arguments to process!\n");
	}
	else {
		// Iterate over all remaining files
		for (; args->i < args->c; ++args->i) {
			char* filename = args->v[args->i];
			int e = pfn(pak, filename);
			// Propagate errors
			if (e) err = e;
		}
	}

	return err;
}
// Callback for each argument with editing
static int PAKtool_edit(PAKtool_args* args, PAK* pak, PAK_unit reserve, int(*pfn)(PAK* pak, PAK_edit* edit, char* filename))
{
	int err = 0;

	if (args->i == args->c) {
		// No arguments to process, this is most likely not intended
		printf("PAKtool: No arguments to process!\n");
	}
	else {
		PAK_edit* edit = PAK_begin(pak, reserve, &PAK_rand);
		if (edit) {
			// Iterate over all remaining files
			for (; args->i < args->c; ++args->i) {
				char* filename = args->v[args->i];
				int e = pfn(pak, edit, filename);
				// Propagate errors
				if (e) err = e;
			}
			PAK_end(edit);
		}
		else {
			printf("PAKtool: Failed to edit.\n");
			err = 1;
		}
	}

	return err;
}

//----------------------------------------------------------------

int PAKtool_touch(PAKtool_args* args)
{
	int err = 1;
	// Open and erase any existing file
	FILE* file = fopen(args->file, "wb");
	if (file) {
		// Create an empty PAK file
		PAK* pak = PAK_create(&args->key, &PAK_rand, file);
		if (pak) {
			PAK_close(pak);
			printf("PAKtool: Created empty PAK file.\n", args->file);
			err = 0;
		}
		else {
			printf("PAKtool: Failed to create PAK file!\n", args->file);
		}
		fclose(file);
	}
	return err;
}

//----------------------------------------------------------------

int PAKtool_fsck(PAKtool_args* args, PAK* pak)
{
	if (PAK_fsck(pak)) {
		printf("PAKtool: Passed fsck!\n");
		return 0;
	}
	else {
		printf("PAKtool: Failed fsck!\n");
		return 1;
	}
}

//----------------------------------------------------------------

static void PAKtool_lsRec(PAK* pak, char prefix[256], const PAK_section* dir)
{
	PAK_desc desc;
	PAK_address it;
	if (PAK_first(pak, &desc, &it, dir)) {
		do {
			if (desc.ContentType) {
				// Print information about the file
				char c = (it.Offset + sizeof(PAK_desc) == (dir->Range.Offset + dir->Range.Size)) ? '*' : '+';
				printf("%s%c %s\n", prefix, c, desc.FileName);
			}
			else {
				// Print information about the directory
				PAK_unit next = it.Offset + sizeof(PAK_desc) + desc.ContentSize;
				char c = (next == dir->Range.Size) ? '*' : '+';
				printf("%s%c %s/\n", prefix, c, desc.FileName);
				// Add to prefix
				int add = (c == '*') ? *(const int*)"  \0\0" : *(const int*)"| \0\0";
				strcat(prefix, (char*)&add);
				// Recurse for subdirectory
				PAKtool_lsRec(pak, prefix, &desc.Section);
				prefix[strlen(prefix) - 2] = 0;
			}
		} while (PAK_next(pak, &desc, &it, dir));
	}
}
int PAKtool_ls(PAKtool_args* args, PAK* pak)
{
	char prefix[256] = { 0 };

	printf("root/\n", &prefix);

	PAKtool_lsRec(pak, prefix, PAK_root(pak));

	return 0;
}

//----------------------------------------------------------------

static int PAKtool_catFile(PAK* pak, char* filename)
{
	PAK_desc desc;
	if (PAK_find(pak, &desc, filename)) {
		// Allocate memory to read whole file
		if (char* data = (char*)malloc(desc.ContentSize + 1)) {
			// Decrypt the file contents
			PAK_address addr = { 0, desc.ContentSize };
			if (PAK_udec(pak, &desc.Section, &addr, data)) {
				data[desc.ContentSize] = 0;
				printf("%s\n", data);
				free(data);
				return 0;
			}
		}
		printf("PAKtool: Failed to decrypt '%s'!\n", filename);
		return 1;
	}
	else {
		printf("PAKtool: File not found '%s'!\n", filename);
		return 1;
	}
}
int PAKtool_cat(PAKtool_args* args, PAK* pak)
{
	return PAKtool_each(args, pak, &PAKtool_catFile);
}

//----------------------------------------------------------------

static int PAKtool_addFile(PAK* pak, PAK_edit* edit, char* filename)
{
	// Format - [dest:]src
	char* dest = filename;
	char* src = strchr(dest, ':');
	if (!src) {
		src = dest;
	}
	else {
		*src++ = 0;
	}
	// Try to read the file
	FILE* file = fopen(src, "rb");
	if (file) {
		// Get the filesize
		fseek(file, 0, SEEK_END);
		long size = ftell(file);
		rewind(file);
		// Add the file descriptor
		PAK_section sec;
		if (PAK_add(edit, 1, size, size, &sec, dest)) {
			// Process file in chunks
			unsigned char data[2048];
			memset(data, 0, sizeof(data));
			PAK_address block = { 0, sizeof(data) };
			for (PAK_unit end = sec.Range.Size; block.Offset < end; block.Offset += sizeof(data)) {
				// Last iteration, only process trailing bytes
				if ((end - block.Offset) < sizeof(data)) {
					block.Size = end - block.Offset;
				}
				fread(data, 1, block.Size, file);
				PAK_aenc(pak, &sec, &block, data);
			}
			fclose(file);
			printf("PAKtool: Added file '%s'.\n", dest, src);
			return 0;
		}
		fclose(file);
		printf("PAKtool: Failed to add '%s'!\n", dest);
		return 1;
	}
	printf("PAKtool: Failed to read '%s'!\n", src);
	return 1;
}
int PAKtool_add(PAKtool_args* args, PAK* pak)
{
	// Number of files to add (+ some extra for directories)
	unsigned numf = args->c - args->i + 10;

	return PAKtool_edit(args, pak, numf, &PAKtool_addFile);
}

//----------------------------------------------------------------

static int PAKtool_rmFile(PAK* pak, PAK_edit* edit, char* filename)
{
	if (PAK_rm(edit, filename)) {
		printf("PAKtool: Deleted '%s'.\n", filename);
		return 0;
	}
	else {
		printf("PAKtool: File not found: '%s'!\n", filename);
		return 1;
	}
}

int PAKtool_rm(PAKtool_args* args, PAK* pak)
{
	return PAKtool_edit(args, pak, 0, &PAKtool_rmFile);
}

//----------------------------------------------------------------

static int PAKtool_mvFile(PAK* pak, PAK_edit* edit, char* filename)
{
	// Format - src:dest
	char* src = filename;
	char* dest = strchr(src, ':');
	if (!dest) {
		printf("PAKtool: Invalid arguments.\n");
		return 1;
	}
	else {
		*dest++ = 0;
	}

	if (PAK_mv(edit, dest, src)) {
		printf("PAKtool: Moved '%s' to '%s'.\n", src, dest);
		return 0;
	}
	else {
		printf("PAKtool: File not found: '%s'.\n", src);
		return 1;
	}
}
int PAKtool_mv(PAKtool_args* args, PAK* pak)
{
	// Number of files to add (+ some extra for directories)
	unsigned numf = args->c - args->i + 10;

	return PAKtool_edit(args, pak, numf, &PAKtool_mvFile);
}

//----------------------------------------------------------------

static int PAKtool_unpackFile(PAK* pak, char* filename)
{
	// Format - src[:dest]
	char* src = filename;
	char* dest = strchr(src, ':');
	if (!dest) {
		dest = src;
	}
	else {
		*dest++ = 0;
	}
	// Find the file in the PAK
	PAK_desc desc;
	if (PAK_find(pak, &desc, src)) {
		// Open the destination for writing
		FILE* file = fopen(dest, "wb");
		if (file) {
			// Process file contents in chunks
			unsigned char data[2048];
			PAK_address block = { 0, sizeof(data) };
			for (PAK_unit end = desc.ContentSize; block.Offset < end; block.Offset += sizeof(data)) {
				// Last iteration, only process trailing bytes
				if ((end - block.Offset) < sizeof(data)) {
					block.Size = end - block.Offset;
				}
				PAK_adec(pak, &desc.Section, &block, data);
				fwrite(data, block.Size, 1, file);
			}
			fclose(file);
			printf("PAKtool: Unpacked '%s' to '%s'.\n", src, dest);
			return 0;
		}
		else {
			printf("PAKtool: Failed to open '%s' for writing!\n", dest);
			return 1;
		}
	}
	else {
		printf("PAKtool: File not found: '%s'!\n", src);
		return 1;
	}
}
int PAKtool_unpack(PAKtool_args* args, PAK* pak)
{
	return PAKtool_each(args, pak, &PAKtool_unpackFile);
}

//----------------------------------------------------------------

int PAKtool_gc(PAKtool_args* args, PAK* pak)
{
	PAK_edit* edit = PAK_begin(pak, 0, &PAK_rand);
	if (edit) {
		PAK_gc(edit);
		printf("PAKtool: GC done!\n");
		PAK_end(edit);
		return 0;
	}
	else {
		printf("PAKtool: Failed to edit.\n");
		return 1;
	}
}

}
