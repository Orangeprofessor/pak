
#ifdef WIN32
#define _CRT_RAND_S
#endif
#include <cstdio>
#include <cstdlib>
#include <cstring>

#ifdef WIN32
#include <io.h>
#endif

#include "pak.h"

extern "C"
{

//----------------------------------------------------------------
// XXTEA Crypto from https://en.wikipedia.org/wiki/XXTEA

#define DELTA 0x9e3779b9
#define MX (((z>>5^y<<2) + (y>>3^z<<4)) ^ ((sum^y) + (key[(p&3)^e] ^ z)))
static void btea_enc(unsigned int* __restrict v, unsigned n, const unsigned int* __restrict key)
{
	unsigned int y, z, sum;
	unsigned p, rounds, e;
	rounds = 16 + 52 / n;
	sum = 0;
	z = v[n - 1];
	do {
		sum += DELTA;
		e = (sum >> 2) & 3;
		for (p = 0; p < n - 1; p++) {
			y = v[p + 1];
			z = v[p] += MX;
		}
		y = v[0];
		z = v[n - 1] += MX;
	} while (--rounds);
}
static void btea_dec(unsigned int* __restrict v, unsigned n, const unsigned int* __restrict key)
{
	unsigned int y, z, sum;
	unsigned p, rounds, e;
	rounds = 16 + 52 / n;
	sum = rounds*DELTA;
	y = v[0];
	do {
		e = (sum >> 2) & 3;
		for (p = n - 1; p > 0; p--) {
			z = v[p - 1];
			y = v[p] -= MX;
		}
		z = v[n - 1];
		y = v[0] -= MX;
		sum -= DELTA;
	} while (--rounds);
}
#undef MX
#undef DELTA

inline void PAK_xor(PAK_block* __restrict a, const PAK_block* __restrict b)
{
	a->words[0] ^= b->words[0];
	a->words[1] ^= b->words[1];
	a->words[2] ^= b->words[2];
	a->words[3] ^= b->words[3];
}
inline void PAK_enc(const PAK_key* key, const PAK_block* __restrict iv, PAK_block* __restrict block)
{
	PAK_xor(block, iv);
	btea_enc(block->words, PAK_BLOCKSIZE/4, key->words);
}
inline void PAK_dec(const PAK_key* key, const PAK_block* __restrict iv, PAK_block* __restrict block)
{
	btea_dec(block->words, PAK_BLOCKSIZE/4, key->words);
	PAK_xor(block, iv);
}

//----------------------------------------------------------------
// Creating the PAK struct

struct PAK_vtable
{
	bool(*read)(PAK* pak, const PAK_address* addr, void* buf);
	bool(*write)(PAK* pak, const PAK_address* addr, const void* buf);
};

struct PAK
{
	const PAK_vtable* vtable;
	void* handle;
	PAK_key key;
	PAK_header hdr;
	bool lock;
};

static bool PAK_writeHeader(PAK* pak)
{
	// Encrypt the header
	PAK_header hdr = pak->hdr;
	PAK_enc(&pak->key, &hdr.IV, &hdr.IV + 1);
	PAK_enc(&pak->key, &hdr.IV + 1, &hdr.IV + 2);
	// Write header to file
	PAK_address addr = { 0, sizeof(hdr) };
	return pak->vtable->write(pak, &addr, &hdr);
}
static bool PAK_readHeader(PAK* pak)
{
	// Read the header from file
	PAK_address addr = { 0, sizeof(PAK_header) };
	if (pak->vtable->read(pak, &addr, &pak->hdr)) {
		// Decrypt the header
		PAK_dec(&pak->key, &pak->hdr.IV + 1, &pak->hdr.IV + 2);
		PAK_dec(&pak->key, &pak->hdr.IV, &pak->hdr.IV + 1);
		return true;
	}
	return false;
}

static bool PAK_readFile(PAK* pak, const PAK_address* addr, void* buf)
{
	FILE* file = (FILE*)pak->handle;
	return fseek(file, addr->Offset, SEEK_SET) == 0 && fread(buf, addr->Size, 1, file) == 1;
}
static bool PAK_writeFile(PAK* pak, const PAK_address* addr, const void* buf)
{
	FILE* file = (FILE*)pak->handle;
	return fseek(file, addr->Offset, SEEK_SET) == 0 && fwrite(buf, addr->Size, 1, file) == 1;
}

PAK* PAK_create(const PAK_key* key, PAK_randfn rand, FILE* file)
{
	static const PAK_vtable vtable = {
		&PAK_readFile,
		&PAK_writeFile,
	};

	// Start at the beginning
	rewind(file);

	PAK* pak = (PAK*)malloc(sizeof(PAK));
	if (pak) {
		// Initialize
		pak->vtable = &vtable;
		pak->handle = file;
		pak->key = *key;
		pak->lock = false;
		// Create an empty header
		rand(&pak->hdr.IV);
		pak->hdr.Version = PAK_VERSION;
		pak->hdr.TotalSize = sizeof(pak->hdr);
		rand(&pak->hdr.Root.IV);
		pak->hdr.Root.Range.Offset = sizeof(pak->hdr);
		pak->hdr.Root.Range.Size = 0;
		// Write the header
		if (PAK_writeHeader(pak)) {
			return pak;
		}
		// This may happen if we didn't have write permissions on the file...
		free(pak);
		pak = nullptr;
	}
	return pak;
}

PAK* PAK_file(const PAK_key* key, FILE* file)
{
	static const PAK_vtable vtable = {
		&PAK_readFile,
		&PAK_writeFile,
	};

	// Start at the beginning
	rewind(file);

	PAK* pak = (PAK*)malloc(sizeof(PAK));
	if (pak) {
		// Initialize
		pak->vtable = &vtable;
		pak->handle = file;
		pak->key = *key;
		pak->lock = false;
		// Cache copy of PAK header
		if (PAK_readHeader(pak) && pak->hdr.Version == PAK_VERSION) {
			return pak;
		}
		// Invalid PAK header
		PAK_close(pak);
		pak = nullptr;
	}
	return pak;
}

static bool PAK_readMem(PAK* pak, const PAK_address* addr, void* buf)
{
	// Check address bounds
	if (addr->Offset > pak->hdr.TotalSize || (addr->Offset + addr->Size) > pak->hdr.TotalSize) {
		return false;
	}

	// Copy memory
	memcpy(buf, (const unsigned char*)pak->handle + addr->Offset, addr->Size);
	return true;
}

PAK* PAK_memory(const PAK_key* key, const void* begin, const void* end)
{
	static const PAK_vtable vtable = {
		&PAK_readMem,
		nullptr,
	};

	// Preserve sanity, limit to 32bit size.
	size_t size = (const unsigned char*)end - (const unsigned char*)begin;
	if (size<sizeof(PAK_header) || static_cast<PAK_unit>(size) != size) {
		return nullptr;
	}

	PAK* pak = (PAK*)malloc(sizeof(PAK));
	if (pak) {
		// Initialize
		pak->vtable = &vtable;
		pak->handle = (void*)begin;
		pak->key = *key;
		pak->lock = false;
		// Partially initialize the header for PAK_readMem requirements
		pak->hdr.TotalSize = static_cast<PAK_unit>(size);
		// Cache copy of PAK header and verify total size
		if (PAK_readHeader(pak) && size == pak->hdr.TotalSize && pak->hdr.Version == PAK_VERSION) {
			return pak;
		}
		PAK_close(pak);
		pak = nullptr;
	}
	return pak;
}

void PAK_close(PAK* pak)
{
	if (pak->lock) {
		// You forgot to call PAK_end, the PAK file is now corrupt :)!
	}
	// Avoid leaking crypto keys and other details
	// FIXME! See http://www.daemonology.net/blog/2014-09-04-how-to-zero-a-buffer.html
	//        Ctrl-f "memset" for other functions with the same issue.
	memset(pak, 0, sizeof(PAK));
	free(pak);
}

//----------------------------------------------------------------
// Crypto abstraction

#ifdef WIN32
bool PAK_rand(PAK_block* out)
{
	// Windows provides easy secure random numbers through rand_s
	// http://msdn.microsoft.com/en-us/library/sxtz2fa8.aspx
	return
		!rand_s(&out->words[0]) &&
		!rand_s(&out->words[1]) &&
		!rand_s(&out->words[2]) &&
		!rand_s(&out->words[3]);
}
#endif

inline bool PAK_contains(const PAK_section* sec, const PAK_address* addr, PAK_unit align = 0)
{
	return
		(!align || !(addr->Offset%align) && !(addr->Size%align)) &&
		addr->Offset <= sec->Range.Size &&
		(addr->Offset + addr->Size) <= sec->Range.Size &&
		!(sec->Range.Offset%PAK_BLOCKSIZE) &&
		!(sec->Range.Size%PAK_BLOCKSIZE);
}

bool PAK_adec(PAK* pak, const PAK_section* sec, const PAK_address* addr, void* buf)
{
	// Must be block aligned and contained inside the section
	if (!PAK_contains(sec, addr, PAK_BLOCKSIZE)) {
		return false;
	}

	// Copy encrypted datas
	PAK_address g_addr = { sec->Range.Offset + addr->Offset, addr->Size };
	if (!pak->vtable->read(pak, &g_addr, buf)) {
		return false;
	}

	// Decrypt using CTR mode
	PAK_unit ctr = addr->Offset;
	for (auto it = (PAK_block*)buf, end = (PAK_block*)((char*)it + addr->Size); it != end; ++it, ctr += sizeof(PAK_block)) {
		PAK_block block = sec->IV;
		block.words[1] += ctr;
		btea_enc(block.words, PAK_BLOCKSIZE / 4, pak->key.words);
		PAK_xor(it, &block);
	}

	return true;
}
bool PAK_udec(PAK* pak, const struct PAK_section* sec, const PAK_address* addr_, void* buf)
{
	PAK_block temp;
	PAK_address addr = *addr_;

	// Start unaligned
	if ((addr.Offset%PAK_BLOCKSIZE) != 0) {
		PAK_unit startb = addr.Offset%PAK_BLOCKSIZE;
		// Read the whole block
		PAK_address pre = { addr.Offset - startb, sizeof(temp) };
		if (!PAK_adec(pak, sec, &pre, &temp)) {
			return false;
		}
		// Calc number of output bytes
		PAK_unit numb = PAK_BLOCKSIZE - startb;
		// Requested size is smaller than one block
		if (addr.Size > numb) {
			addr.Size = numb;
		}
		// Output requested bytes
		unsigned char* out = (unsigned char*)buf;
		for (unsigned i = 0; i < numb; ++i) {
			out[i] = temp.bytes[startb + i];
		}
		// Request is smaller than one block
		if (addr.Size == numb) {
			return true;
		}
		// Advance counters
		buf = out + numb;
		addr.Offset += numb;
		addr.Size -= numb;
	}

	// Calculate trailing bytes
	PAK_unit anumb = addr.Size&~(PAK_BLOCKSIZE - 1);
	PAK_unit uendb = addr.Size - anumb;

	if (anumb) {
		// Read aligned bytes
		addr.Size = anumb;
		if (!PAK_adec(pak, sec, &addr, buf)) {
			return false;
		}
		// Advance counters
		buf = (unsigned char*)buf + anumb;
		addr.Offset += anumb;
		addr.Size = uendb;
	}

	// Read trailing bytes
	if (uendb) {
		// Read the whole block
		addr.Size = sizeof(temp);
		if (!PAK_adec(pak, sec, &addr, &temp)) {
			return false;
		}
		// Output trailing bytes
		unsigned char* out = (unsigned char*)buf;
		for (unsigned i = 0; i < uendb; ++i) {
			out[i] = temp.bytes[i];
		}
	}
	return true;
}
bool PAK_aenc(PAK* pak, const struct PAK_section* sec, const PAK_address* addr, void* buf)
{
	// Must be block aligned AND have writing capabilities
	if (!PAK_contains(sec, addr, PAK_BLOCKSIZE) || !pak->vtable->write) {
		return false;
	}

	// Encrypt using CTR mode
	PAK_unit ctr = addr->Offset;
	for (auto it = (PAK_block*)buf, end = (PAK_block*)((char*)it + addr->Size); it != end; ++it, ctr += sizeof(PAK_block)) {
		PAK_block block = sec->IV;
		block.words[1] += ctr;
		btea_enc(block.words, PAK_BLOCKSIZE / 4, pak->key.words);
		PAK_xor(it, &block);
	}

	// Write encoded contents to file
	PAK_address g_addr = { sec->Range.Offset + addr->Offset, addr->Size };
	return pak->vtable->write(pak, &g_addr, buf);
}

//------------------------------------------------
// Reading from PAK files

const PAK_section* PAK_root(PAK* pak)
{
	// Not available while locked
	return pak->lock ? nullptr : &pak->hdr.Root;
}

bool PAK_first(struct PAK* pak, struct PAK_desc* desc, struct PAK_address* it, const struct PAK_section* dir)
{
	// Cannot find files while locked
	if (pak->lock) {
		return false;
	}

	// Fetch first entry (bounds check done by PAK_adec)
	it->Offset = 0;
	it->Size = sizeof(PAK_desc);

	if (PAK_adec(pak, dir, it, desc)) {
		// Fixup section for directory
		if (!desc->ContentType) {
			desc->Section.Range.Offset = dir->Range.Offset + it->Offset + sizeof(PAK_desc);
			desc->Section.Range.Size = desc->ContentSize;
			desc->Section.IV = dir->IV;
			desc->Section.IV.words[1] += it->Offset + sizeof(PAK_desc);
		}
		return true;
	}
	return false;
}
bool PAK_next(struct PAK* pak, struct PAK_desc* desc, struct PAK_address* it, const struct PAK_section* dir)
{
	// Cannot find files while locked
	if (pak->lock) {
		return false;
	}

	it->Offset += sizeof(PAK_desc);
	if (!desc->ContentType) {
		it->Offset += desc->ContentSize;
	}
	if (PAK_adec(pak, dir, it, desc)) {
		// Fixup section for directory
		if (!desc->ContentType) {
			desc->Section.Range.Offset = dir->Range.Offset + it->Offset + sizeof(PAK_desc);
			desc->Section.Range.Size = desc->ContentSize;
			desc->Section.IV = dir->IV;
			desc->Section.IV.words[1] += it->Offset + sizeof(PAK_desc);
		}
		return true;
	}
	return false;
}

static const char* PAK_namecmp(const PAK_desc* desc, const char* filename)
{
	unsigned int i = 0;
	char a, b;
	do {
		a = desc->FileName[i];
		b = filename[i];
		if (!a) {
			if (!b) {
				// Final exact match
				return filename + i;
			}
			else if (b == '/' || b == '\\') {
				// Match but we have a subdirectory to look for
				return filename + i + 1;
			}
			// No exact match
		}
	} while (++i, a == b);
	// Mismatch in filenames
	return nullptr;
}
bool PAK_find(PAK* pak, PAK_desc* desc, const char* filename, const PAK_section* root)
{
	// Cannot find files while locked
	if (pak->lock) {
		return false;
	}

	// Locate the directory listing
	if (!root) {
		root = &pak->hdr.Root;
	}

	// Address of first descriptor
	PAK_address dir = { 0, root->Range.Size };
	PAK_address it = { dir.Offset, sizeof(PAK_desc) };

	// Iterate over files
	for (; it.Offset < (dir.Offset + dir.Size); it.Offset += sizeof(PAK_desc)) {
		// Decrypt the file descriptor
		if (!PAK_adec(pak, root, &it, desc)) {
			return false;
		}

		// Filename compare
		if (const char* subname = PAK_namecmp(desc, filename)) {
			// Exact final match found
			if (!*subname) {
				if (!desc->ContentType) {
					// Fixup directory section to act as an indepedent root
					desc->Section.Range.Offset = root->Range.Offset + it.Offset + sizeof(PAK_desc);
					desc->Section.Range.Size = desc->ContentSize;
					desc->Section.IV = root->IV;
					desc->Section.IV.words[1] += it.Offset + sizeof(PAK_desc);
				}
				return true;
			}
			// If this is a subdirectory
			if (!desc->ContentType) {
				// Set new directory constraints
				dir.Offset = it.Offset + sizeof(PAK_desc);
				dir.Size = desc->ContentSize;
				// Traverse subdirectory
				filename = subname;
				continue;
			}
			// Not a directory
			return false;
		}

		// If this is a directory
		if (!desc->ContentType) {
			// Skip all the descriptors part of this directory
			it.Offset += desc->ContentSize;
		}
	}
	// Not found
	return false;
}

bool PAK_fsck(PAK* pak, const PAK_address* dir)
{
	// Not available while locked
	if (pak->lock) {
		return false;
	}

	// Start at the root
	PAK_address dir_root;
	if (!dir) {
		dir = &pak->hdr.Root.Range;

		// Version check
		if (!(pak->hdr.Version == PAK_VERSION))
			return false;
		// TotalSize must be aligned
		if (!(!(pak->hdr.TotalSize%PAK_BLOCKSIZE)))
			return false;

		// Root offset range check
		if (!(dir->Offset <= pak->hdr.TotalSize))
			return false;
		// Root must be the last thing
		if (!((dir->Offset + dir->Size) == pak->hdr.TotalSize))
			return false;
		// Root must be aligned
		if (!(!(dir->Size%PAK_BLOCKSIZE)))
			return false;

		// Root to local address
		dir = &dir_root;
		dir_root.Offset = 0;
		dir_root.Size = pak->hdr.Root.Range.Size;
	}

	// Walk the directory entries
	PAK_address it = { dir->Offset, sizeof(PAK_desc) };

	while (it.Offset < (dir->Offset + dir->Size)) {
		PAK_desc desc;
		PAK_adec(pak, &pak->hdr.Root, &it, &desc);

		if (desc.ContentType) {
			// Address must start after the header
			if (!(desc.Section.Range.Offset >= sizeof(PAK_header)))
				return false;
			// Address must end before the directory
			if (!((desc.Section.Range.Offset + desc.Section.Range.Size) <= pak->hdr.Root.Range.Offset))
				return false;
			// Address must be aligned
			PAK_address addr = { 0, desc.ContentSize };
			if (!PAK_contains(&desc.Section, &addr))
				return false;
			// Next descriptor
			it.Offset += sizeof(PAK_desc);
		}
		else {
			// Section must be zero
			if (!(desc.Section.Range.Offset == 0) || !(desc.Section.Range.Size == 0) )
				return false;
			// Size must be aligned
			if (!(!(desc.ContentSize%sizeof(PAK_desc))))
				return false;
			// Fixup offset
			desc.Section.Range.Offset = it.Offset + sizeof(PAK_desc);
			desc.Section.Range.Size = desc.ContentSize;
			// Subdirectory must end before the parent ends
			if (!((desc.Section.Range.Offset + desc.Section.Range.Size) <= (dir->Offset + dir->Size)))
				return false;
			// Recursively fsck this directory
			if (!PAK_fsck(pak, &desc.Section.Range))
				return false;
			// Next descriptor
			it.Offset += sizeof(PAK_desc) + desc.Section.Range.Offset;
		}
	}
	// Passed all the tests!
	return true;
}

//------------------------------------------------
// Mutate an existing PAK file

struct PAK_edit
{
	// PAK we're editing
	PAK* pak;
	// Random number generator source
	PAK_randfn rand;
	// File position to put next files
	PAK_unit next;
	// Number of descriptors below
	PAK_unit numd;
	// Max number of descriptors
	PAK_unit maxd;
	// Descriptors, extends beyond this declaration!
	PAK_desc desc[1];
};

PAK_edit* PAK_begin(PAK* pak, PAK_unit reserve, PAK_randfn rand)
{
	// Fail if unwritable or already locked
	if (!pak->vtable->write || pak->lock) {
		return nullptr;
	}
	// Total number of descriptors we'll be allocating
	PAK_unit numd = pak->hdr.Root.Range.Size / sizeof(PAK_desc);
	PAK_edit* edit = (PAK_edit*)malloc(sizeof(PAK_edit) + sizeof(PAK_desc)*(numd + reserve));
	if (edit) {
		edit->pak = pak;
		edit->rand = rand;
		edit->numd = numd;
		edit->maxd = numd + reserve;
		// Overwrite the descriptors while working
		edit->next = pak->hdr.Root.Range.Offset;
		// Read all the descriptors
		PAK_address addr = { 0, pak->hdr.Root.Range.Size };
		if (!numd || PAK_adec(pak, &pak->hdr.Root, &addr, &edit->desc)) {
			// Lock the PAK while editing
			pak->lock = true;
			return edit;
		}
		// This shouldn't happen
		free(edit);
		edit = nullptr;
	}
	return edit;
}

static int PAK_gc_sortfn(const void* a, const void* b)
{
	const PAK_desc* pa = *(const PAK_desc**)a;
	const PAK_desc* pb = *(const PAK_desc**)b;

	return pa->Section.Range.Offset - pb->Section.Range.Offset;
}
bool PAK_gc(PAK_edit* edit)
{
	// Read all *file* descriptors
	size_t descsize = sizeof(PAK_desc**) * edit->numd;
	PAK_desc** descs = (PAK_desc**)malloc(descsize);
	if (!descs) {
		return false;
	}
	PAK_unit numf, i;
	for (numf = 0, i = 0; i < edit->numd; ++i) {
		// If descriptor is a file descriptor, save it
		if (edit->desc[i].ContentType) {
			descs[numf++] = &edit->desc[i];
		}
	}
	// Sort by address
	qsort(descs, numf, sizeof(*descs), &PAK_gc_sortfn);
	// Find first garbage
	PAK_unit next = sizeof(PAK_header);
	for (auto it = descs, end = it + numf; it < end; ++it) {
		PAK_address* addr = &(*it)->Section.Range;
		// If file needs to be moved
		if (addr->Offset != next) {
			// Process in chunks of 2048 bytes
			unsigned char data[2048];
			PAK_address src = { addr->Offset, sizeof(data) };
			PAK_address dest = { next, sizeof(data) };
			for (PAK_unit end = addr->Offset + addr->Size; src.Offset < end; src.Offset += sizeof(data), dest.Offset += sizeof(data)) {
				// Last iteration, only process trailing bytes
				if ((end - src.Offset) < sizeof(data)) {
					src.Size = end - src.Offset;
					dest.Size = src.Size;
				}
				// Read file contents
				edit->pak->vtable->read(edit->pak, &src, data);
				// Write to new location
				// (note! if this fails we completely corrupted the file... Maybe write to a new file instead?)
				edit->pak->vtable->write(edit->pak, &dest, data);
			}
			// Update the file address
			addr->Offset = next;
		}
		// Next offset
		next += addr->Size;
	}
	// Update the descriptors offset
	edit->next = next;
	// Free memory
	memset(descs, 0, descsize);
	free(descs);
	return true;
}

static void PAK_alloc(PAK_edit* edit, PAK_unit reserve, PAK_section* sec)
{
	// Keep things aligned
	reserve = (reserve + PAK_BLOCKSIZE - 1) & ~(PAK_BLOCKSIZE - 1);
	// Simple bump allocation
	sec->Range.Offset = edit->next;
	sec->Range.Size = reserve;
	edit->rand(&sec->IV);
	// Next free
	edit->next += reserve;
}

static PAK_desc* PAK_inc(PAK_edit* edit, const char** dirname, const int* inc, bool exists)
{
	// This function doesn't really make a new directory;
	// It just increases the Size of all directory descriptors in dirname by inc
	// It returns the descriptor to start inserting new descriptors
	// The filename part is returned by updating dirname
	// Not sure what exists does

	PAK_desc* it = edit->desc;
	PAK_desc* end = edit->desc + edit->numd;
	for (; it != end; ++it) {
		if (const char* subname = PAK_namecmp(it, *dirname)) {
			if (!*subname) {
				// File exists!
				return exists ? it : nullptr;
			}
			// If this is a directory
			if (!it->ContentType) {
				// Step into this directory
				*dirname = subname;
				end = (PAK_desc*)((char*)it + sizeof(PAK_desc) + it->ContentSize);
				// Increment the size for this directory
				it->ContentSize += *inc * sizeof(PAK_desc);
				continue;
			}
			// Not a directory
			return nullptr;
		}

		// Skip the following directory
		if (!it->ContentType) {
			it = (PAK_desc*)((char*)it + it->ContentSize);
		}
	}
	return exists ? nullptr : it;
}

static bool PAK_flenck(const char* path, int* inc, unsigned* flen)
{
	// Returns false if any part of the path is too long
	// Returns the depth of the path in inc (including filename itself)
	// Returns the filename length in flen (0 if used to add a directory)
	// FIXME! Ugly code...
	unsigned fbegin = 0;
	unsigned fi = 0;
	for (; path[fi]; ++fi) {
		if (path[fi] == '/' || path[fi] == '\\') {
			(*inc)++;
			if ((fi - fbegin) >= sizeof(((PAK_desc*)0)->FileName)) {
				return false;
			}
			fbegin = fi + 1;
		}
	}
	*flen = (fi - fbegin);
	if (*flen >= sizeof(((PAK_desc*)0)->FileName)) {
		return false;
	}
	if (flen) {
		(*inc)++;
	}
	return true;
}

bool PAK_add(PAK_edit* edit, PAK_unit type, PAK_unit size, PAK_unit reserve, PAK_section* sec, const char* filename)
{
	// Dry-run to check stuff
	int inc = 0;
	const char* dirname = filename;
	if (!PAK_inc(edit, &dirname, &inc, false)) {
		return false;
	}

	// Filename length check + how many descriptors to add
	unsigned flen;
	if (!PAK_flenck(dirname, &inc, &flen)) {
		return false;
	}
	// Sanity check;
	if (!(type && flen || !type && !flen)) {
		return false;
	}
	// When adding a file given existing section, the size must be <= section Range.Size
	if (type && !reserve && size > sec->Range.Size) {
		return false;
	}

	// This is triggered if all you want is to add a directory that already exists
	if (!inc) {
		return true;
	}

	// Check if we have enough space to add new descriptors
	if (edit->numd + inc > edit->maxd) {
		return false;
	}

	// Increase the size of the directory descriptors
	dirname = filename;
	PAK_desc* it = PAK_inc(edit, &dirname, &inc, false);

	// Move other entries down
	size_t mbytes = ((edit->desc + edit->numd) - it)*sizeof(PAK_desc);
	memmove(it + inc, it, mbytes);

	// Clear the descriptors we'll be filling in
	edit->numd += inc;
	memset(it, 0, inc*sizeof(PAK_desc));

	// Insert directories
	for (; --inc; ++it) {
		it->ContentType = 0;
		it->ContentSize = inc * sizeof(PAK_desc);
		unsigned di;
		for (di = 0; dirname[di] != '/' && dirname[di] != '\\'; ++di) {
			it->FileName[di] = dirname[di];
		}
		dirname += di + 1;
	}

	if (flen) {
		// Allocate if requested
		if (reserve) {
			// Preserve sanity
			if (reserve < size) {
				reserve = size;
			}
			// Allocate and provide a copy to caller
			PAK_alloc(edit, reserve, &it->Section);
			if (sec) {
				*sec = it->Section;
			}
		}
		// Assign existing section (allows efficient renames)
		else if (sec) {
			it->Section = *sec;
		}
		// Empty file with nothing allocated
		else {
		}
		// Write the file descriptor
		it->ContentType = type;
		it->ContentSize = size;
		for (unsigned di = 0; dirname[di]; ++di) {
			it->FileName[di] = dirname[di];
		}
	}
	else {
		// Write the directory descriptor
		for (unsigned di = 0; dirname[di] != '/' && dirname[di] != '\\'; ++di) {
			it->FileName[di] = dirname[di];
		}
	}

	return true;
}

bool PAK_rm(PAK_edit* edit, const char* filename, PAK_desc* deleted)
{
	// Dry-run to check stuff
	int inc = 0;
	const char* dirname = filename;
	if (!PAK_inc(edit, &dirname, &inc, true)) {
		return false;
	}

	// Decrease size of the directory descriptors
	inc = -1;
	dirname = filename;
	PAK_desc* it = PAK_inc(edit, &dirname, &inc, true);

	// Save a copy of the descriptor being deleted if requested
	if (deleted) {
		*deleted = *it;
	}

	// Move other entries up
	size_t mbytes = ((edit->desc + edit->numd) - (it - inc))*sizeof(PAK_desc);
	memmove(it, it - inc, mbytes);
	edit->numd += inc;

	return true;
}

bool PAK_mv(PAK_edit* edit, const char* dest, const char* src)
{
	// Start by deleting the descriptor
	PAK_desc desc;
	if (PAK_rm(edit, src, &desc)) {
		// Re-add the file
		if (PAK_add(edit, desc.ContentType, desc.ContentSize, 0, &desc.Section, dest)) {
			return true;
		}
		// If that fails, re-add with as the original source
		else if (PAK_add(edit, desc.ContentType, desc.ContentSize, 0, &desc.Section, src)) {
			return false;
		}
		// Should never fail, in theory :)
		else {
		}
	}
	return false;
}

void PAK_end(PAK_edit* edit)
{
	PAK* pak = edit->pak;

	// Create a new header
	edit->rand(&pak->hdr.IV);
	pak->hdr.Root.Range.Offset = edit->next;
	pak->hdr.Root.Range.Size = edit->numd*sizeof(PAK_desc);
	pak->hdr.TotalSize = pak->hdr.Root.Range.Offset + pak->hdr.Root.Range.Size;
	edit->rand(&pak->hdr.Root.IV);
	PAK_writeHeader(pak);

	// Encrypt the descriptors
	PAK_address addr = { 0, pak->hdr.Root.Range.Size };
	PAK_aenc(pak, &pak->hdr.Root, &addr, edit->desc);

	// Trim the file
#ifdef WIN32
	_chsize(_fileno((FILE*)edit->pak->handle), pak->hdr.TotalSize);
#endif

	// Free memory
	memset(edit, 0, sizeof(PAK_edit));
	free(edit);

	// Unlock the pak
	pak->lock = false;
}

}
