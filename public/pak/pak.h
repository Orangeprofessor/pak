#pragma once

/// \file
///----------------------------------------------------------------
/// Custom PAK Format
///----------------------------------------------------------------
/// Stores files in an 128 bit encrypted store.

typedef struct _iobuf FILE;

#ifdef __cplusplus
extern "C" {
#define def(x) = x
#else
#define bool unsigned char
#define def(x)
#endif

//------------------------------------------------

enum {
	/// Block size (128 bit).
	/// Changing this requires a pretty serious redesign of the structures.
	PAK_BLOCKSIZE = 128 / 8,
	/// Key size (128 bit).
	PAK_KEYSIZE = 128 / 8,
	/// File format version.
	/// Increment on incompatible format changes.
	PAK_VERSION = 1,
};

/// Block units (16 bytes).
/// Represents a cryptographic block. All encrypted data should be a multiple of its size.
struct PAK_block
{
	union {
		unsigned int words[PAK_BLOCKSIZE/4];
		unsigned char bytes[PAK_BLOCKSIZE];
	};
};

/// Crypto keys (128 bit).
/// Represents a cryptographic key.
struct PAK_key
{
	union {
		unsigned int words[PAK_KEYSIZE/4];
		unsigned char bytes[PAK_KEYSIZE];
	};
};

/// This was meant to represent file offsets and sizes in case dword isn't enough, but ended up being used as a generic replacement for unsigned int.
typedef unsigned int PAK_unit;

/// Address fat pointer.
struct PAK_address
{
	/// Offset from the start of the section this address is being used in.
	PAK_unit Offset;
	/// Size of the data referred to.
	PAK_unit Size;
};

/// Section.
struct PAK_section
{
	/// Global address inside the PAK file this section applies to.
	struct PAK_address Range;
	/// Initializing Vector used by the crypto.
	/// Each section gets its own IV to make it easier to edit parts of the PAK file without breaking encryption by IV reuse.
	struct PAK_block IV;
};

/// PAK file header.
struct PAK_header
{
	/// Initializing Vector for the header.
	struct PAK_block IV;
	/// Version identifier, must match `::PAK_VERSION`.
	PAK_unit Version;
	/// Total size of the PAK including the header.
	PAK_unit TotalSize;
	/// Section containing the directory listing.
	struct PAK_section Root;
};

/// Opaque PAK context.
struct PAK;

//------------------------------------------------
// PAK Crypto

/// Random number provider.
/// Used to generate random IVs when creating and editing the PAK.
typedef bool(*PAK_randfn)(struct PAK_block* iv);

/// Default secure random number provider.
///
/// \param[out] iv
///   Random IV is assigned.
///
/// \return
///   Returns false if no randomness is available, this error is unrecoverable and will propagate up the caller.
bool PAK_rand(struct PAK_block* iv);

/// Read and decrypt to buffer.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \param[in] sec
///   Section being read from. The range must be aligned to PAK_BLOCKSIZE.
///
/// \param[in] addr
///   Address to read from, Offset and Size must be aligned to PAK_BLOCKSIZE.
///   The address is relative to the section and must be fully contained inside it, an Offset of 0 means the start of the section.
///
/// \param[out] buf
///   Output buffer containing the decrypted contents, must be at least addr->Size bytes long.
///
/// \return
///   Returns false if unaligned, out of bounds or unable to read the requested data.
bool PAK_adec(struct PAK* pak, const struct PAK_section* sec, const struct PAK_address* addr, void* buf);

/// Read and decrypt to buffer.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \param[in] sec
///   Section being read from. The range must be aligned to PAK_BLOCKSIZE.
///
/// \param[in] addr
///   Address to read from.
///   The address is relative to the section and must be fully contained inside it, an Offset of 0 means the start of the section.
///
/// \param[out] buf
///   Output buffer containing the decrypted contents, must be at least addr->Size bytes long.
///
/// \return
///   Returns false if out of bounds or unable to read the requested data.
bool PAK_udec(struct PAK* pak, const struct PAK_section* sec, const struct PAK_address* addr, void* buf);

/// Encrypt buffer in place and write to file.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \param[in] sec
///   Section being written to. The range must be aligned to `::PAK_BLOCKSIZE`.
///
/// \param[in] addr
///   Address to write to, Offset and Size must be aligned to `::PAK_BLOCKSIZE`.
///   The address is relative to the section and must be fully contained inside it, an Offset of 0 means the start of the section.
///
/// \param[in,out] buf
///   Output buffer containing the contents, must be at least addr->Size bytes long. Is overwritten in place with the encrypted data.
///
/// \return
///   Returns false if unaligned, out of bounds or unable to write the requested data.
bool PAK_aenc(struct PAK* pak, const struct PAK_section* sec, const struct PAK_address* addr, void* buf);

//------------------------------------------------
// Creating the PAK struct

/// Initialize to an empty PAK file.
///
/// \param[in] key
///   Cryptographic key, a copy is kept.
///
/// \param[in] rand
///   Random number provider. A default secure implementation is provided as `PAK_rand()`.
///
/// \param[in] file
///   File should be opened with `"w+b"` for read+write permissions in binary mode; does `rewind(file)`.
///   The FILE handle must remain valid for the lifetime of the returned PAK context.
///
/// \return
///   If successful, the PAK context must be freed with `PAK_close()`.
struct PAK* PAK_create(const struct PAK_key* key, PAK_randfn rand, FILE* file);

/// PAK file on disk.
///
/// \param[in] key
///   Cryptographic key, a copy is kept.
///
/// \param[in] file
///   File should be opened with `"r+b"` for read+write permissions in binary mode, `"rb"` is sufficient if no editing is required; does `rewind(file)`.
///   The FILE handle must remain valid for the lifetime of the returned PAK handle.
///
/// \return
///   If successful, the PAK context must be freed with `PAK_close()`.
struct PAK* PAK_file(const struct PAK_key* key, FILE* file);

/// PAK file in memory.
///
/// \param[in] key
///   Cryptographic key, a copy is kept.
///
/// \param[in] begin
///   Beginning of the PAK data. It must be kept valid for the lifetime of the returned PAK context.
///
/// \param[in] end
///   End of the PAK data.
///
/// \return
///   If successful, the PAK handle must be freed with `PAK_close`.
struct PAK* PAK_memory(const struct PAK_key* key, const void* begin, const void* end);

/// Finish with the PAK file and free memory.
/// If the PAK was created as a file, it can now be closed.
///
/// \param[in,out] pak
///   Opaque PAK context.
void PAK_close(struct PAK* pak);

//------------------------------------------------
// Reading from PAK files

/// File and subdirectory descriptor.
struct PAK_desc
{
	/// Type of the content.
	/// 0 = Directory, anything else = specifies user defined content/encoding.
	PAK_unit ContentType;
	/// Actual size of the content, no alignment requirements.
	PAK_unit ContentSize;
	/// Section the content is part of.
	/// Content starts at the beginning of this section up to ContentSize, the remaining padding is undefined (probably zeroes).
	struct PAK_section Section;
	/// Name of the descriptor.
	char FileName[64];
};

/// Get the section of the directory.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \return
///   May return nullptr if the PAK is being edited.
const struct PAK_section* PAK_root(struct PAK* pak);

/// Begin iterating over files in a directory.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \param[out] desc
///   Returns the first descriptor in the directory.
///
/// \param[out] it
///   Returns the initial value of the iterator. No need to touch this, just pass it on to `PAK_next()`.
///
/// \param[in] dir
///   Directory to iterate over.
///   Accepts the return value of `PAK_root()` to start from the root, or use `PAK_desc::Section` after `PAK_find()` on a directory.
///   Please check that you are feeding it a directory section and not a file section as this is not verified.
///
/// \return
///   Returns false on error or when all descriptors have passed.
bool PAK_first(struct PAK* pak, struct PAK_desc* desc, struct PAK_address* it, const struct PAK_section* dir);

/// Next descriptor when iterating over a directory.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \param[out] desc
///   Returns the next descriptor in the directory.
///
/// \param[in,out] it
///   Updated iterator. No need to touch this, just pass it on.
///
/// \param[in] dir
///   Directory that is being iterated over, must be the same as passed to `PAK_first()`.
///
/// \return
///   Returns false on error or when all descriptors have passed.
bool PAK_next(struct PAK* pak, struct PAK_desc* desc, struct PAK_address* it, const struct PAK_section* dir);

/// Search for a descriptor by name.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \param[out] desc
///   If found, returns the descriptor for the filename.
///
/// \param[in] filename
///   Name of the case-sensitive file or directory to search for, both forward- and backslashes are interpreted as delimiters.
///
/// \param[in] root
///   Directory listing, nullptr uses default root.
///
/// \return
///   False if error reading or filename isn't found.
bool PAK_find(struct PAK* pak, struct PAK_desc* desc, const char* filename, const struct PAK_section* root def(nullptr));

/// Check the PAK for consistency.
///
/// \param[in] pak
///   Opaque PAK context.
///
/// \param[in] dir
///   Internal use, must be nullptr.
///
/// \return
///   Returns if the PAK was successfully verified.
bool PAK_fsck(struct PAK* pak, const struct PAK_address* dir def(nullptr));

//------------------------------------------------
// Mutating existing PAK files

/// Opaque edit context.
struct PAK_edit;

/// Begin editing the PAK.
///
/// \param[in] pak
///   Opaque PAK context, it must remain valid for the lifetime of the edit context.
///   Failure to do so will irrecoverably corrupt the PAK.
///
/// \param[in] reserve
///   How many extra descriptors to reserve for adding new directories and files.
///
/// \param[in] rand
///   Random number provider.
///
/// \return
///   If successful, the edit contest must be freed with `PAK_end()`.
///   Failure to do so will irrecoverably corrupt the PAK file as the descriptors have been temporarily overwritten by new files.
struct PAK_edit* PAK_begin(struct PAK* pak, PAK_unit reserve, PAK_randfn rand);

/// Garbage collect the PAK.
///
/// This function will collect the garbage by rewriting all the contents sequentially. Very expensive.
/// When a file is removed, its contents remain. The IVs for their sections are destroyed, the contents are unrecoverable.
///
/// *WARNING!* The way this works will edit the file in place, if an error occurs (read/write failure) will result in unrecoverable data loss.
///
/// \param[in] edit
///   Opaque edit context.
///
/// \return
///   Returns false if disaster happened, the file is now unrecoverably corrupted.
bool PAK_gc(struct PAK_edit* edit);

/// Add a descriptor.
///
/// \param[in] edit
///   Opaque edit context.
///
/// \param[in] type
///   The ContentType for the new file, must be zero for a directory.
///
/// \param[in] size
///   The ContentSize for the new file, ignored when `type` is zero.
///
/// \param[in] reserve
///   The size of the section allocated for the new file, ignored when `type` is zero.
///   When nonzero is rounded up to `::PAK_BLOCKSIZE` and made >= `size`.
///   When zero and adding a file descriptor (nonzero `type`), no space is allocated and the existing `sec` section is used as input; fails if `size` does not fit in the `sec` section.
///
/// \param[in,out] sec
///   This argument is interpreted in 3 different ways depending on the other arguments:
///   When adding directories (zero `type`) this is ignored and can be nullptr.
///   When adding a file (nonzero `type`) and `reserve` is zero, will use this section as input instead of allocating space, this enables efficient moving files around.
///   Otherwise the newly allocated section is returned here, ready to have its file contents written to.
///
/// \param[in] filename
///   The name of the file to add. Subdirectories are added as needed.
///   Filenames ending with a forward- or backslash are interpreted as directories, otherwise assumed a filename.
///   Subdirectories are added as necessary, fails if there isn't enough room for the new descriptors (as passed to `PAK_begin()`'s `reserve` argument).
///
/// \return
///   False on failure.
bool PAK_add(struct PAK_edit* edit, PAK_unit type, PAK_unit size, PAK_unit reserve, struct PAK_section* sec, const char* filename);

/// Remove a descriptor.
///
/// \param[in] edit
///   Opaque edit context.
///
/// \param[in] filename
///   Case-sensitive filename to remove.
///   When removing a directory, its children are moved up a directory. It does not recursively delete the descriptors, no such functionality is provided at this time.
///
/// \param[out] deleted
///   Optional; returns the removed descriptor to enable efficient renames.
///
/// \return
///   False on failure.
bool PAK_rm(struct PAK_edit* edit, const char* filename, struct PAK_desc* deleted def(nullptr));

/// Rename a file descriptor.
///
/// \param[in] edit
///   Opaque edit context.
///
/// \param[in] dest
///   New filename.
///
/// \param[in] src
///   Name of the case-sensitive filename to rename.
///
/// \return
///   If false, disaster may have happened (in which case the file is irrecoverably lost), more likely is that the src filename does not exist or dest is not a valid destination.
bool PAK_mv(struct PAK_edit* edit, const char* dest, const char* src);

/// End editing and save the descriptors.
/// Failure to call this function will irrecoverably corrupt the PAK as the directory has not been saved.
///
/// The PAK context can now be safely `PAK_close()`'d.
///
/// \param[in,out] edit
///   Opaque edit context.
void PAK_end(struct PAK_edit* edit);

#ifdef __cplusplus
}
#else
#undef bool
#endif
#undef def
