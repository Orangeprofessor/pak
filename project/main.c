
#include <stdio.h>
#include <stdlib.h>

#include "../public/pak/paktool.h"

int main(int argc, char* argv[])
{
	struct PAKtool_args args = { 1, argc, argv };
	int ret;

	ret = PAKtool_parse(&args);
	if (!args.cmd) {
		return ret;
	}

	ret = PAKtool_invoke(&args);
	return ret;
}
