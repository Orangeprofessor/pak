@ECHO off
REM /* Copy and rename the compiled binary to the ..\bin\ folder */

SET target=%1
SET ext=%2
SET config=%3
SET platform=%4

SET source=%config%\%target%%ext%
IF %platform%==x64 SET source=x64\%source%

SET dest=..\bin\%target%
IF %platform%==x64 SET dest=%dest%64
IF %config%==Debug SET dest=%dest%d
SET dest=%dest%%ext%

COPY /Y /B %source% %dest%
